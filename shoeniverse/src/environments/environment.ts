// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCBtEtp3wxeFj8olJ3b6kuH2QEnox2rCqU",
    authDomain: "shoeniverse-b0a2a.firebaseapp.com",
    databaseURL: "https://shoeniverse-b0a2a-default-rtdb.firebaseio.com",
    projectId: "shoeniverse-b0a2a",
    storageBucket: "shoeniverse-b0a2a.appspot.com",
    messagingSenderId: "521872807409",
    appId: "1:521872807409:web:5a4583408a7d2f60bae050"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
