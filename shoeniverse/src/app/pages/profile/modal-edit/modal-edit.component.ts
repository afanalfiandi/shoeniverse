import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-modal-edit',
  templateUrl: './modal-edit.component.html',
  styleUrls: ['./modal-edit.component.scss'],
})
export class ModalEditComponent implements OnInit {
  currentModal = null;
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) { }

  ngOnInit() {}
  dismissModal() {
    this.modalCtrl.dismiss();
  }

  onSubmit() {
    this.modalCtrl.dismiss();
    this.navCtrl.navigateRoot('/pages/tabs/profile');
  }
}
