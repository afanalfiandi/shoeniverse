import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ModalEditComponent } from './modal-edit/modal-edit.component';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  isLoading = false;
  constructor(
    public navCtrl: NavController,
    public router: Router,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  onCart() {
    this.navCtrl.navigateRoot('/pages/tabs/cart');
  }

  async onEdit() {
    const modal = await this.modalCtrl.create({
      component: ModalEditComponent,
      cssClass: 'edit-modal',
      swipeToClose: true,
    });
    return await modal.present();
  }

  onWishlist() {
    this.navCtrl.navigateRoot('/pages/tabs/wishlist');
  }

  async onPhoto() {
    const modal = await this.modalCtrl.create({
      component: EditProfileComponent,
      cssClass: 'photo-modal',
      swipeToClose: true,
    });
    return await modal.present();
  }
  async onLogout() {
    this.isLoading = true;
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Logging out . . .' })
      .then((loadingEl) => {
        loadingEl.present();
        setTimeout(() => {
          this.isLoading = false;
          loadingEl.dismiss();
          this.router.navigateByUrl('/login-page');
        }, 1500);
      });
  }
}
