import { Component, OnInit, ViewChild } from '@angular/core';
import {
  IonSlides,
  MenuController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { ProductService } from 'src/app/products/product.service';
import { BrandModalPage } from './brand-modal/brand-modal.page';
import { PopoverController } from '@ionic/angular';
import { NotificationComponent } from './notification/notification.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  segment = 0;
  selectedSlide: any;

  @ViewChild('slides', { static: false }) slider: IonSlides;

  slide = [];
  product = [];
 

  constructor(
    private productSvc: ProductService,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController
  ) {}
  slideOpts = {
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,
    spaceBetween: 0.5,
  };

  sliderOpt = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 400,
  };
  sld = {
    slidesPerView: 2,
    spaceBetween: 0.1,
  };

  ngOnInit() {
    this.slide = this.productSvc.getSlides();
    this.product = this.productSvc.getProduct();
  }

  async segmentChanged(ev: any) {
    this.slider.slideTo(this.segment);
  }

  async slidesChanged(slides: IonSlides) {
    
  }

  async presentModal(name, brandImg, brandImgd, price, description) {
    const modal = await this.modalCtrl.create({
      component: BrandModalPage,
      cssClass: 'my-modal-class',
      componentProps: {
        name: name,
        brandImg: brandImg, 
        brandImgd: brandImgd,
        price: price,
        description: description
      },
    });
    return await modal.present();
  }

  async openMenu() {
    await this.menuCtrl.open();
  }

  async onNotification(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationComponent,
      cssClass: 'pop-over',
      event: ev,
      translucent: true,
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
