import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-brand-modal',
  templateUrl: './brand-modal.page.html',
  styleUrls: ['./brand-modal.page.scss'],
})
export class BrandModalPage implements OnInit {
  @Input() firstName: string;
  @Input() lastName: string;
  @Input() middleInitial: string;
  
  currentModal = null;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController) { }

  ngOnInit() {
  }
  dismissModal() {
    this.modalCtrl.dismiss();
  }
  onAdd() {
    this.modalCtrl.dismiss();
    this.navCtrl.navigateRoot('/pages/tabs/cart');
  }
}
