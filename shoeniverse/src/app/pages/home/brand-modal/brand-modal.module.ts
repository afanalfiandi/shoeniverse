import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrandModalPageRoutingModule } from './brand-modal-routing.module';

import { BrandModalPage } from './brand-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrandModalPageRoutingModule
  ],
  declarations: [BrandModalPage]
})
export class BrandModalPageModule {}
