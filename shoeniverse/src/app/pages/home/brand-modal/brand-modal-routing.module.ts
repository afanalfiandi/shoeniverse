import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrandModalPage } from './brand-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BrandModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrandModalPageRoutingModule {}
