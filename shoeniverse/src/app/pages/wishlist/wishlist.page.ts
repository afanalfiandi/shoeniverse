import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  onWishlist() {
    this.navCtrl.navigateRoot('/pages/tabs/cart');
  }
}
