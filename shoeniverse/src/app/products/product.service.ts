import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private slides = [
    {
      id: '1',
      brand: 'New Ballance',
      img: './assets/img/slider/slider-1.png',
      motto: 'Declare Your Victory',
    },
    {
      id: '2',
      brand: 'Nike. Just Do It.',
      img: './assets/img/slider/slider-2.png',
      motto: 'A Heritage Of Speed',
    },
    {
      id: '3',
      brand: 'Converse',
      img: './assets/img/slider/slider-3.png',
      motto: 'Shoes Are Boring. Wear Sneakers',
    },
    {
      id: '4',
      brand: 'Adidas',
      img: './assets/img/slider/slider-4.png',
      motto: 'Nothing Impossible',
    },
  ];

  private product = [
    {
      value: '0',
      cat: 'New Ballance',
      product: [
        {
          brandId: 'nb1',
          name: 'F34RUNEW0 Black',
          brandImg: './assets/img/products/nb/F34RUNEW0-Black.png',
          brandImgd: './assets/img/products/nb/F34RUNEW0-Black-d.png',
          price: 1499,
          description: 'Our flexible and fashionable Playgruv running shoe is designed for active kids. A lightweight sole unit provides comfortable support for running, jumping, kicking and playing. Kids and parents alike will appreciate how the unique rope lace and bungee strap closure system helps ensure a snug fit while still allowing room for growing feet to flex. These kids’ running shoes are crafted with a synthetic and mesh upper for lightweight durability and all-day comfort.'
        },
        {
          brandId: 'nb2',
          name: 'FFSSBNEW0 Green',
          brandImg: './assets/img/products/nb/FFSSBNEW0-Green.png',
          brandImgd: './assets/img/products/nb/FFSSBNEW0-Green-d.png',
          price: 2199,
          description: 'Taking cues from our heritage designs, these casual sneakers for women blend a retro look with modern styling for a versatile and stylish silhouette that keeps your streetwear looking sharp. The suede and mesh upper with lace-up construction provides a secure, comfortable all-day fit.'
        },
        {
          id: 'nb3',
          name: 'FFSSBNEW0 Grey',
          brandImg: './assets/img/products/nb/FFSSBNEW0-Grey.png',
          brandImgd: './assets/img/products/nb/FFSSBNEW0-Grey-d.png',
          price: 4759,
          description: 'The FuelCell Propel RMX offers a louder, fresher take on your favorite running shoes. A progressive upper made from a combination of sandwich mesh, translucent textile and stitched-down synthetic overlays adds depth and head-turning style. Built on a full-length FuelCell midsole, this mens running shoe not only feels amazing underfoot, but delivers incredible rebound to propel you forward with each stride.'
        },
      ],
    },
    {
      value: '1',
      cat: 'Nike',
      product: [
        {
          brandId: 'n1',
          name: 'Air Jordan Mid',
          brandImg: './assets/img/products/nike/air-jordan-mid.png',
          brandImgd: './assets/img/products/nike/air-jordan-mid-d.png',
          price: 3199,
          description: 'The groundbreaking Air Jordan 3 emerged during Michael Jordans 88th season with eye-catching prints and visible Air. This version is a crossover over from court to street with modernised style while still maintaining true heritage.'
        },
        {
          brandId: 'n2',
          name: 'Blazer Mid 7 Vintage Shoe',
          brandImg: './assets/img/products/nike/blazer-mid-77-vintage-shoe.png',
          brandImgd: './assets/img/products/nike/blazer-mid-77-vintage-shoe-d.png',
          price: 3299,
          description: 'In the 70s, Nike was the new shoe on the block. So new in fact, we were still breaking into the basketball scene and testing prototypes on the feet of our local team. Of course, the design improved over the years, but the name stuck. The Nike Blazer Mid 77 Vintage—classic from the beginning.'
        },
        {
          brandId: 'n3',
          name: 'Custom Nike Blazer 77 Cozi',
          brandImg: './assets/img/products/nike/custom-nike-blazer-77-cozi.png',
          brandImgd: './assets/img/products/nike/custom-nike-blazer-77-cozi-d.png',
          price: 4199,
          description: 'Put your own spin on the Nike Blazer Mid 77, a wardrobe staple ready for winter. Make a one-of-a-kind, hoops-inspired icon thats yours and yours alone by putting your own touch on the upper, sole and other details. Warm, insulated fill in key areas adds classic cold-weather appeal.'
        },
      ],
    },
    {
      value: '2',
      cat: 'Converse',
      product: [
        {
          brandId: 'c1',
          name: 'Chuck 70',
          brandImg: './assets/img/products/converse/chuck-70.png',
          brandImgd: './assets/img/products/converse/chuck-70-d.png',
          price: 999,
          description: 'High-top, premium shoe with 100% recycled canvas upperVintage star ankle patch and rubber heel plate ties in 70s heritageGlossy, rubber sidewall and premium cotton laces elevate the styleReinforced tongue stitching helps keep it in placeOrthoLite'

        },
        {
          brandId: 'c2',
          name: 'All Star 70s High',
          brandImg: './assets/img/products/converse/chuck-taylor-all-star-70s-high.png',
          brandImgd: './assets/img/products/converse/chuck-taylor-all-star-70s-high-d.png',
            price: 879,
            description: 'The iconic 70s design plays with sparkle, brightening winter style with a sparkle that makes a statement. The all-over glitter design gets another flash from the aluminum eyelet. With premium Chuck 70 details, such as a glossy midsole, vintage star ankle patch and extra cushioning. Seen. Listen.'
        },
        {
          brandId: 'c3',
          name: 'All Star Hi-Lifes Too Short to Waste',
          brandImg: './assets/img/products/converse/chuck-taylor-all-star-hi-lifes-too-short-to-waste.png',
          brandImgd: './assets/img/products/converse/chuck-taylor-all-star-hi-lifes-too-short-to-waste-d.png',
            price: 765,
            description: 'High-top shoe with recycled PET knit upper made from >35% recycled content by weightBreathable, stretchy engineered knit upper is made from approximately 75% recycled polyester and 25% nylon100% polyester lace closure75% recycled OrthoLite'
        },
      ],
    },
    {
      value: '3',
      cat: 'Adidas',
      product: [
        {
          brandId: 'a1',
          name: 'Adidas ULTRABOOST',
          brandImg: './assets/img/products/adidas/B42200_SLC.png',
          brandImgd: './assets/img/products/adidas/B42200_SLC-d.png',
          price: 799,
          description: 'The coastal city is one of the most beautiful cities in the world. You can almost feel the sea breeze and hear the soft rustle of tropical palm trees when you wear these running shoes. The adidas Primeknit upper conforms to your foot for a supportive feel, and Boost keeps you going wherever you are.'
        },
        {
          brandId: 'a2',
          name: 'Adidas ZX ALKYNE',
          brandImg: './assets/img/products/adidas/fx8384_slc.png',
          brandImgd: './assets/img/products/adidas/fx8384_slc-d.png',
            price: 989,
            description: 'Where you end up matters. But how you get there matters more. The ZX line has always been a laboratory for innovation. These adidas shoes are the latest step forward on a journey of constant improvement. Every day, a little bit better. Every year, a little bit farther. Every step, a new horizon.'
        },
        {
          brandId: 'a3',
          name: 'Adidas ZX 10000',
          brandImg: './assets/img/products/adidas/fz2559_sl.png',
          brandImgd: './assets/img/products/adidas/fz2559_sl-d.png',
          price: 960,
          description: 'The Krusty Burger sneaker is part of the A-ZX series. The bold lines, embroidery, chenille, and lace jewels are an homage to The Simpsons beloved restaurant.'
        },
      ],
    },
  ];
  constructor() {}

  getSlides() {
    return this.slides;
  }

  getProduct() {
    return this.product;
  }
}
