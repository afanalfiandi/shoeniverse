import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { LoginService } from './login.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  userForm: FormGroup;
  successMsg: string = '';
  errorMsg: string = '';
  signUpForm: FormGroup;
  error_msg = {
    email: [
      {
        type: 'required',
        message: 'Provide email.',
      },
      {
        type: 'pattern',
        message: 'Email is not valid.',
      },
    ],
    password: [
      {
        type: 'required',
        message: 'Password is required.',
      },
      {
        type: 'minlength',
        message: 'Password length should be 6 characters long.',
      },
    ],
  };

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private loginService: LoginService,
    private fb: FormBuilder
  ) {}
  isLogin = true;
  isShow = true;
  isLoading = false;
  ngOnInit() {
    this.userForm = this.fb.group({
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([Validators.minLength(6), Validators.required])
      ),
    });

    this.signUpForm = this.fb.group({
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([Validators.minLength(6), Validators.required])
      ),
    });
  }
  signUp(value) {
    this.loginService.createUser(value).then(
      (response) => {
        this.errorMsg = '';
        this.successMsg = 'New user created.';
      },
      (error) => {
        this.errorMsg = error.message;
        this.successMsg = '';
      }
    );
  }

  signIn(value) {
    this.loginService.signinUser(value).then(
      (response) => {
        console.log(response);
        this.errorMsg = '';
        this.isLoading = true;
        this.loadingCtrl
          .create({ keyboardClose: true, message: 'Logging in . . .' })
          .then((loadingEl) => {
            loadingEl.present();
            setTimeout(() => {
              this.isLoading = false;
              loadingEl.dismiss();
              this.router.navigateByUrl('pages/tabs/home');
            }, 1500);
          });
      },
      (error) => {
        this.errorMsg = error.message;
        this.successMsg = '';
      }
    );
  }

  
  /** without firebase */
  onSwitch() {
    this.isShow = !this.isShow;
  }

  onLogin() {
    this.isLoading = true;
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Logging in . . .' })
      .then((loadingEl) => {
        loadingEl.present();
        setTimeout(() => {
          this.isLoading = false;
          loadingEl.dismiss();
          this.router.navigateByUrl('pages/tabs/home');
        }, 1500);
      });
  }

  onSignup() {
    this.isShow = !this.isShow;
  }
}
