import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
  LoadingController,
  MenuController,
  NavController,
  PopoverController,
} from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isLoading = false;

  constructor(
    private router: Router,
    public menuCtrl: MenuController,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController
  ) {}

  async onDismissMenu() {
    this.menuCtrl.close();
  }

  onOrders() {
    this.menuCtrl.close();
    this.navCtrl.navigateRoot('/pages/tabs/cart');
  }
  onSettings() {
    this.menuCtrl.close();
    this.navCtrl.navigateRoot('/pages/tabs/profile');
  }
  
  async onLogout() {
    this.menuCtrl.close();
    this.isLoading = true;
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Logging out . . .' })
      .then((loadingEl) => {
        loadingEl.present();
        setTimeout(() => {
          this.isLoading = false;
          loadingEl.dismiss();
          this.router.navigateByUrl('/get-started');
        }, 1500);
      });
  }
}
